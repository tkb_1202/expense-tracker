package com.fivepointsomeone.expensetracker.datamodels;

public class CategoryExpenseSummary {

    private String categoryName;
    private float lastMonthTransactions;
    private float lastWeekTransactions;

    public CategoryExpenseSummary(float lastMonthTransactions, float lastWeekTransactions) {
        this.lastMonthTransactions = lastMonthTransactions;
        this.lastWeekTransactions = lastWeekTransactions;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public float getLastMonthTransactions() {
        return lastMonthTransactions;
    }

    public void setLastMonthTransactions(float lastMonthTransactions) {
        this.lastMonthTransactions = lastMonthTransactions;
    }

    public float getLastWeekTransactions() {
        return lastWeekTransactions;
    }

    public void setLastWeekTransactions(float lastWeekTransactions) {
        this.lastWeekTransactions = lastWeekTransactions;
    }
}
