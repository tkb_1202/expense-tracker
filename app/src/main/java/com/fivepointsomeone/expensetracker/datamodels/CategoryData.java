package com.fivepointsomeone.expensetracker.datamodels;

public class CategoryData {

    private String name;
    private String type;

    private CategoryExpenseSummary categoryExpenseSummary;

    public CategoryData(String name, String type, CategoryExpenseSummary categoryExpenseSummary) {
        this.name = name;
        this.type = type;
        this.categoryExpenseSummary = categoryExpenseSummary;
    }

    public CategoryData(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CategoryExpenseSummary getCategoryExpenseSummary() {
        return categoryExpenseSummary;
    }

    public void setCategoryExpenseSummary(CategoryExpenseSummary categoryExpenseSummary) {
        this.categoryExpenseSummary = categoryExpenseSummary;
    }
}
