package com.fivepointsomeone.expensetracker.datamodels;

public class Transaction {
    private String  date;
    private String time;
    private String reason;
    private float amount;
    private String category;
    private String account;
    private String timeStamp;

    public Transaction(String date, String time, String reason, float amount, String category, String account) {
        setDate(date);
        setTime(time);
        setReason(reason);
        setAmount(amount);
        setCategory(category);
        setAccount(account);
    }

    public Transaction(String date, String time, String reason, float amount, String category, String account, String timeStamp) {
        setDate(date);
        setTime(time);
        setReason(reason);
        setAmount(amount);
        setCategory(category);
        setAccount(account);
        setTimeStamp(timeStamp);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
//        return super.toString();
        String data = "Date: " + getDate() + "\n" +
                    "Time: " + getTime() + "\n" +
                    "Reason: " + getReason() + "\n" +
                    "Amount: " + Float.toString(getAmount()) + "\n" +
                    "Category: " + getCategory() + "\n" +
                    "Account: " + getAccount() + "\n" +
                    "TimeStamp: " + getTimeStamp() + "\n";

        return data;

    }
}
