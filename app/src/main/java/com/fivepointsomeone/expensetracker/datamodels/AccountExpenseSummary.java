package com.fivepointsomeone.expensetracker.datamodels;

public class AccountExpenseSummary {

    private float lastMonthDebit;
    private float lastWeekDebit;
    private float lastMonthCredit;
    private float lastWeekCredit;

    private float lastWeekNetExpense;
    private float lastMonthNetExpense;

    private AccountExpenseSummary accountExpenseSummary;

    public AccountExpenseSummary(float lastMonthDebit, float lastWeekDebit, float lastMonthCredit, float lastWeekCredit) {
        setLastMonthDebit(lastMonthDebit);
        setLastWeekDebit(lastWeekDebit);
        setLastMonthCredit(lastMonthCredit);
        setLastWeekCredit(lastWeekCredit);
        setLastMonthNetExpense(getLastMonthDebit() - getLastMonthCredit());
        setLastWeekNetExpense(getLastWeekDebit() - getLastWeekCredit());

    }

    public float getLastMonthDebit() {
        return lastMonthDebit;
    }

    public void setLastMonthDebit(float lastMonthDebit) {
        this.lastMonthDebit = lastMonthDebit;
    }

    public float getLastWeekDebit() {
        return lastWeekDebit;
    }

    public void setLastWeekDebit(float lastWeekDebit) {
        this.lastWeekDebit = lastWeekDebit;
    }

    public float getLastMonthCredit() {
        return lastMonthCredit;
    }

    public void setLastMonthCredit(float lastMonthCredit) {
        this.lastMonthCredit = lastMonthCredit;
    }

    public float getLastWeekCredit() {
        return lastWeekCredit;
    }

    public void setLastWeekCredit(float lastWeekCredit) {
        this.lastWeekCredit = lastWeekCredit;
    }

    public float getLastWeekNetExpense() {
        return lastWeekNetExpense;
    }

    public void setLastWeekNetExpense(float lastWeekNetExpense) {
        this.lastWeekNetExpense = lastWeekNetExpense;
    }

    public float getLastMonthNetExpense() {
        return lastMonthNetExpense;
    }

    public void setLastMonthNetExpense(float lastMonthNetExpense) {
        this.lastMonthNetExpense = lastMonthNetExpense;
    }
}
