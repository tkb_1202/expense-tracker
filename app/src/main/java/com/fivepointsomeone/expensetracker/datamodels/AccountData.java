package com.fivepointsomeone.expensetracker.datamodels;

public class AccountData {

    private String name;
    private float balance;

    public AccountData(String name, float balance) {
        setName(name);
        setBalance(balance);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float updateBalance(float transactionAmount) {

        setBalance(getBalance() - transactionAmount);
        return getBalance();

    }

}
