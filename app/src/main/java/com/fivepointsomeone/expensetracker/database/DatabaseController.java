package com.fivepointsomeone.expensetracker.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.fivepointsomeone.expensetracker.datamodels.AccountData;
import com.fivepointsomeone.expensetracker.datamodels.CategoryData;
import com.fivepointsomeone.expensetracker.datamodels.Transaction;

import java.util.ArrayList;
import java.util.List;

public class DatabaseController extends SQLiteOpenHelper {



    private static final String LOG_TAG = "DatabaseController";

    private static final String DATABASE_NAME = "database";

    private static final String TABLE_ACCOUNT_NAME = "account_type";
    private static final String TABLE_TRANSACTION__NAME = "transaction_table";
    private static final String TABLE_CATEGORY_NAME = "category_table";

    private static final String COLUMN_TIMESTAMP_NAME = "timestamp";
    private static final String COLUMN_REASON_NAME = "reason";
    private static final String COLUMN_AMOUNT_NAME = "amount";
    private static final String COLUMN_TIME_NAME = "time";
    private static final String COLUMN_DATE_NAME = "date";

    private static final String COLUMN_ACCOUNT_TYPE_NAME = "account_name";
    private static final String COLUMN_BALANCE_NAME = "balance";

    private static final String COLUMN_CATEGORY_NAME = "category_name";
    private static final String COLUMN_CATEGORY_MODE_NAME = "category_type";

    public static DatabaseController dbInstance;

    public static synchronized DatabaseController getInstance(Context context) {
        if (dbInstance == null) {
            dbInstance = new DatabaseController(context.getApplicationContext());
        }
        return dbInstance;
    }


    public DatabaseController(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        createTables(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createTables(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_ACCOUNT_NAME + " ( " + COLUMN_ACCOUNT_TYPE_NAME + " TEXT PRIMARY KEY UNIQUE, " +
                COLUMN_BALANCE_NAME + " DECIMAL(9, 2) )");

        db.execSQL("CREATE TABLE " + TABLE_CATEGORY_NAME + " ( " + COLUMN_CATEGORY_NAME + " TEXT PRIMARY KEY UNIQUE, " +
                COLUMN_CATEGORY_MODE_NAME + " TEXT" + " )");

        db.execSQL("CREATE TABLE " + TABLE_TRANSACTION__NAME + " ( " + COLUMN_TIMESTAMP_NAME + " TEXT PRIMARY KEY, " +
                COLUMN_REASON_NAME + " TEXT, " + COLUMN_CATEGORY_NAME + " TEXT, " + COLUMN_AMOUNT_NAME + " DECIMAL(9,2), " +
                COLUMN_ACCOUNT_TYPE_NAME + " TEXT, " + COLUMN_TIME_NAME + " TEXT, " + COLUMN_DATE_NAME + " TEXT, " +
                "FOREIGN KEY (" + COLUMN_ACCOUNT_TYPE_NAME + ") REFERENCES " + TABLE_ACCOUNT_NAME +
                "(" + COLUMN_ACCOUNT_TYPE_NAME + ") , " + "FOREIGN KEY ( " + COLUMN_CATEGORY_NAME + ") REFERENCES " +
                TABLE_CATEGORY_NAME + "(" + COLUMN_CATEGORY_NAME + ") )");

        Log.i(LOG_TAG, "Database created");

    }

    public List<String> getAccountTypes() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_ACCOUNT_NAME, null);

        List<String>  stringList = new ArrayList<>();
        while(c.moveToNext()) {
            stringList.add(c.getString(0));
        }

        return stringList;
    }

    public List<AccountData> getAccounts() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_ACCOUNT_NAME;
        List<AccountData> list = new ArrayList<>();
        try {

            Cursor c = db.rawQuery(query, null);
            if(c != null) {
                if(c.moveToFirst()) {
                    do {
                        String name = c.getString(c.getColumnIndex(COLUMN_ACCOUNT_TYPE_NAME));
                        float balance = c.getFloat(c.getColumnIndex(COLUMN_BALANCE_NAME));
                        AccountData accountData = new AccountData(name, balance);
                        list.add(accountData);
                    } while (c.moveToNext());
                }
            }

        } catch (SQLiteException e) {
            Log.e(LOG_TAG, "Error while fetching Accounts from Database: " + e.getMessage());
            list = null;
        }
        db.close();
        return list;
    }

    public List<String> getCategoryTypes() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_CATEGORY_NAME, null);
        List<String>  stringList = new ArrayList<>();
        while(c.moveToNext()) {
            stringList.add(c.getString(0));
        }

        return stringList;
    }

    public List<CategoryData> getCategories() {

        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_CATEGORY_NAME;
        List<CategoryData> list = new ArrayList<>();
        try {

            Cursor c = db.rawQuery(query, null);
            if(c.moveToFirst()) {
                do {
                    String name = c.getString(c.getColumnIndex(COLUMN_CATEGORY_NAME));
                    String type = c.getString(c.getColumnIndex(COLUMN_CATEGORY_MODE_NAME));
                    CategoryData categoryData = new CategoryData(name, type);
                    list.add(categoryData);
                } while (c.moveToNext());
            }

        } catch (SQLiteException e) {
            Log.e(LOG_TAG, "Error while fetching Accounts from Database: " + e.getMessage());
            list = null;
        }
        db.close();
        return list;
    }

    public boolean addTransactionToDatabase(Transaction transactionClass) {

        boolean returnBoolean = false;
        SQLiteDatabase db = getWritableDatabase();

        String queryReadAccount = "SELECT " + COLUMN_BALANCE_NAME + " FROM " + TABLE_ACCOUNT_NAME + " WHERE " +
                COLUMN_ACCOUNT_TYPE_NAME + " = " + "\'" +  transactionClass.getAccount() + "\'";
        String queryReadCategory = "SELECT " + COLUMN_CATEGORY_MODE_NAME + " FROM " + TABLE_CATEGORY_NAME + " WHERE " +
                COLUMN_CATEGORY_NAME + " =  \'" + transactionClass.getCategory() + "\'";

        float balance = 0;
        float deductingAmount = 0;
        String category = "";
        try {
            Cursor cursorCategory = db.rawQuery(queryReadCategory, null);
            if(cursorCategory.moveToFirst()) {
                category = cursorCategory.getString(cursorCategory.getColumnIndex(COLUMN_CATEGORY_MODE_NAME));
            } else {
                Log.e(LOG_TAG, "cursorCategory.moveToFirst() == null");
            }
            Cursor cursorAccountBalance = db.rawQuery(queryReadAccount, null);
            if (cursorAccountBalance.moveToFirst()) {
                balance = cursorAccountBalance.getFloat(cursorAccountBalance.getColumnIndex(COLUMN_BALANCE_NAME));
                if(category.equals("CREDIT")) {
                    deductingAmount = -1 * transactionClass.getAmount();
                }
                Log.e(LOG_TAG, "Balance: " + Float.toString(balance));
            } else {
                Log.e(LOG_TAG, "cursorAccountBalance.moveToFirst() == null");
            }
        } catch (SQLiteException e) {
            Log.e(LOG_TAG, "Error while getting balance account while entering transaction: " + e.getMessage());
        }

        String queryWriteAccount = "UPDATE " + TABLE_ACCOUNT_NAME + " SET " + COLUMN_BALANCE_NAME + " = " +
                "\'" + (balance - deductingAmount) + "\'" + " WHERE " + COLUMN_ACCOUNT_TYPE_NAME + " = " +
                "\'"+ transactionClass.getAccount() + "\'";
        Log.i(LOG_TAG, queryWriteAccount);

        String queryWriteTransaction = "INSERT INTO " + TABLE_TRANSACTION__NAME + " ( " + COLUMN_ACCOUNT_TYPE_NAME + "," +
                COLUMN_AMOUNT_NAME + ", " + COLUMN_CATEGORY_NAME + ", " + COLUMN_TIME_NAME + ", " + COLUMN_DATE_NAME + ", " +
                COLUMN_TIMESTAMP_NAME + ", " + COLUMN_REASON_NAME + " )" + " VALUES " + "( " +
                "\'" + transactionClass.getAccount() + "\', \'" + transactionClass.getAmount() + "\', \'" +
                transactionClass.getCategory() + "\', \'" + transactionClass.getTime() + "\', \'" + transactionClass.getDate()
                + "\', DATETIME(\'now\')" + ", " + "\'" + transactionClass.getReason() + "\'" + ");";
        db.beginTransaction();
        try {

            db.execSQL(queryWriteAccount);
            db.execSQL(queryWriteTransaction);
            db.setTransactionSuccessful();
            returnBoolean = true;

        } catch (SQLiteException e) {
            Log.e(LOG_TAG, "Error while inserting transaction: " + e.getMessage());
        } finally {
            db.endTransaction();
        }

        db.close();
        return returnBoolean;

//        readTable(TABLE_TRANSACTION__NAME);
    }

    public boolean addAccountToDatabase(AccountData accountData) {

        SQLiteDatabase db = this.getWritableDatabase();

        String query = "INSERT INTO " + TABLE_ACCOUNT_NAME + "( " + COLUMN_ACCOUNT_TYPE_NAME + ", " + COLUMN_BALANCE_NAME +
                " ) " + "VALUES " +  " (\'" + accountData.getName() + "\' , \'" + accountData.getBalance() + "\' )";

        try {

            db.execSQL(query);
            db.close();
            return true;

        } catch(SQLiteException e) {
            Log.e(LOG_TAG, "Error while inserting account: " + e.getMessage());
            db.close();
            return false;
        }

    }

    public boolean addCategoryToDatabase(CategoryData categoryData) {

        SQLiteDatabase db = getWritableDatabase();

        String query = "INSERT INTO " + TABLE_CATEGORY_NAME + "( " + COLUMN_CATEGORY_NAME + ", " + COLUMN_CATEGORY_MODE_NAME +
                " ) " + "VALUES " +  " (\'" + categoryData.getName() + "\' , \'" + categoryData.getType() + "\' )";

        try {

            db.execSQL(query);
            db.close();
            return true;

        } catch(SQLiteException e) {
            Log.e(LOG_TAG, "Error while inserting account: " + e.getMessage());
            db.close();
            return false;
        }

    }


    private void readTransactionTable(String tableName) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " +  tableName, null);
        if(c.moveToFirst()) {

            do {
                String timeStamp = c.getString(c.getColumnIndex(COLUMN_TIMESTAMP_NAME));
                Log.i(LOG_TAG, timeStamp);
            } while(c.moveToNext());

        }

        db.close();

    }

    public List<Transaction> getDailyExpensesBetweenDays(String beginDate, String endDate) {

        SQLiteDatabase db = this.getReadableDatabase();

        List<Transaction> transactionList = new ArrayList<>();
        Transaction tempTransaction;

        String query = "SELECT * FROM " + TABLE_TRANSACTION__NAME + " WHERE " + COLUMN_DATE_NAME + " BETWEEN " +
                "\'" + beginDate + "\'" + " AND " + "\'" + endDate + "\'";

        Cursor c;

        try {

            c = db.rawQuery(query, null);

            if(c.moveToFirst()) {
                do {

                    tempTransaction = new Transaction(c.getString(c.getColumnIndex(COLUMN_DATE_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_TIME_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_REASON_NAME)), c.getInt(c.getColumnIndex(COLUMN_AMOUNT_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_CATEGORY_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_ACCOUNT_TYPE_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_TIMESTAMP_NAME)));
                    transactionList.add(tempTransaction);
                } while (c.moveToNext());
            }  else {
                Log.i(LOG_TAG, "Cursor came with 0 length in getDailyExpensesBetweenDays() method");
            }

        } catch (SQLException e) {
            Log.e(LOG_TAG, "Error while fetching data in getDailyExpansesBetweenDays(): " + e.getMessage());
            transactionList = null;
        }
        db.close();
        return transactionList;
    }

    public List<Transaction> getDailyExpensesForAllDays() {

        SQLiteDatabase db = this.getReadableDatabase();

        List<Transaction> transactionList = new ArrayList<>();
        Transaction tempTransaction;

        String query = "SELECT * FROM " + TABLE_TRANSACTION__NAME;
        Cursor c;

        try {

            c = db.rawQuery(query, null);

            if(c.moveToFirst()) {
                do {

                    tempTransaction = new Transaction(c.getString(c.getColumnIndex(COLUMN_DATE_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_TIME_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_REASON_NAME)), c.getInt(c.getColumnIndex(COLUMN_AMOUNT_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_CATEGORY_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_ACCOUNT_TYPE_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_TIMESTAMP_NAME)));
                    transactionList.add(tempTransaction);
                } while (c.moveToNext());
            } else {
                Log.i(LOG_TAG, "Cursor came with 0 length in getDailyExpensesForAllDays() method");
            }

        } catch (SQLException e) {
            Log.e(LOG_TAG, "Error while fetching data in getDailyExpansesBetweenDays(): " + e.getMessage());
            transactionList = null;
        }
        db.close();
        return transactionList;
    }

    public List<Transaction> getAllTransactionInDateOrder() {

        SQLiteDatabase db = getReadableDatabase();

        List<Transaction> transactionList = new ArrayList<>();
        Transaction tempTransaction;

//        String query2 = "SELECT * FROM " + TABLE_TRANSACTION__NAME + " ORDER BY " + "DATE(" + COLUMN_DATE_NAME +
// ") ASC LIMIT 1000";
        String query = "SELECT * FROM " + TABLE_TRANSACTION__NAME + " ORDER BY " + COLUMN_DATE_NAME + " ASC, " +
                COLUMN_TIME_NAME + " ASC";
                Cursor c;

        try {

            c = db.rawQuery(query, null);
            int i = 0;

            if(c.moveToFirst()) {
                do {

                    tempTransaction = new Transaction(c.getString(c.getColumnIndex(COLUMN_DATE_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_TIME_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_REASON_NAME)),
                            c.getFloat(c.getColumnIndex(COLUMN_AMOUNT_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_CATEGORY_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_ACCOUNT_TYPE_NAME)),
                            c.getString(c.getColumnIndex(COLUMN_TIMESTAMP_NAME)));
                    transactionList.add(tempTransaction);
//                    Log.i(LOG_TAG, "Transaction " + Integer.toString(i) + tempTransaction.toString());
                } while (c.moveToNext());
            } else {
//                Log.i(LOG_TAG, "Cursor came with 0 length in getDailyExpensesForAllDays() method");
            }

        } catch (SQLException e) {
            Log.e(LOG_TAG, "Error while fetching data in getDailyExpansesBetweenDays(): " + e.getMessage());
            transactionList = null;
        }
        db.close();
//        deleteAllRows(TABLE_TRANSACTION__NAME);
        return transactionList;
    }

    private void deleteAllRows(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM " + tableName;
        try {
            db.execSQL(query);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Error while deleting database rows: " + e.getMessage());
        }
        db.close();
    }
}