package com.fivepointsomeone.expensetracker.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.datamodels.CategoryData;
import com.fivepointsomeone.expensetracker.datamodels.CategoryExpenseSummary;

import java.util.HashMap;
import java.util.List;

public class CategoryExpandableListAdapter extends BaseExpandableListAdapter {

    private List<CategoryData> categoryDataList;
    private HashMap<CategoryData, CategoryExpenseSummary> categoryChildDataList;

    public CategoryExpandableListAdapter(List<CategoryData> categoryDataList, HashMap<CategoryData, CategoryExpenseSummary> categoryChildDataList) {
        this.categoryDataList = categoryDataList;
        this.categoryChildDataList = categoryChildDataList;
    }

    @Override
    public int getGroupCount() {
        return categoryDataList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return categoryDataList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return categoryChildDataList.get(categoryDataList.get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_category_expandable_list_parent, parent, false);
        }

        TextView textViewCategoryName = convertView.findViewById(R.id.textViewCategoryName);
        textViewCategoryName.setText(categoryDataList.get(groupPosition).getName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_category_expandable_list_child, parent, false);
        }

        CategoryExpenseSummary categoryExpenseSummary = categoryChildDataList.get(categoryDataList.get(groupPosition));

        TextView textViewMonthlyTransaction = convertView.findViewById(R.id.textViewMonthlyTransaction);
        TextView textViewWeeklyTransaction = convertView.findViewById(R.id.textViewWeeklyTransaction);

        if(categoryExpenseSummary != null) {
            textViewMonthlyTransaction.setText(Float.toString(categoryExpenseSummary.getLastMonthTransactions()));
            textViewWeeklyTransaction.setText(Float.toString(categoryExpenseSummary.getLastWeekTransactions()));
        } else {
            textViewMonthlyTransaction.setText("0.00");
            textViewWeeklyTransaction.setText("0.00");
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
