package com.fivepointsomeone.expensetracker.utils;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

public class EditTextDecimalLimiterFilter implements InputFilter {

    private int maxDecimalNumbers;

    public EditTextDecimalLimiterFilter(int maxDecimalNumbers) {
        this.maxDecimalNumbers = maxDecimalNumbers;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (source.equals(".") && dest.toString().length() == 0) {
            Log.i("Flag", "FloatFilter: " + "0.");
            return "0.";
        }
        if (dest.toString().contains(".")) {
            int index = dest.toString().indexOf(".");
            int mLength = dest.toString().substring(index).length();
            if (mLength == 3) {
                return "";
            }
        }
        return null;
    }
}
