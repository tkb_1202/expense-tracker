package com.fivepointsomeone.expensetracker.utils;

import android.util.Log;

public class DateAndTimeManipulateFactory {

    private static final String LOG_TAG = "DateAndTimeManipulat";

    public static String fromSystemToUITime(int hour, int minute) {
        String timeString = "";
        if(hour <= 9) {
            timeString = "0" + Integer.toString(hour);
        } else {
            timeString = Integer.toString(hour);
        }
        timeString = timeString + ":";
        if(minute <= 9) {
            timeString = timeString + "0" + Integer.toString(minute);
        } else {
            timeString = timeString + Integer.toString(minute);
        }
        Log.i(LOG_TAG,timeString);

        return timeString;
    }

    public static String fromSystemToUIDateWithYear(int year, int month, int day) {

        String dateString = "";

        if(day <= 9) {
            dateString = "0" + Integer.toString(day);
        } else {
            dateString = Integer.toString(day);
        }
        dateString = dateString + " ";

        String monthInString = GlobalDataHolder.getMonthArray()[month - 1];
        dateString = dateString + monthInString;
        dateString = dateString + " " + Integer.toString(year);

        return dateString;

    }

    public static String fromDbToUIDateWithYear(String date) {
        String[] phrases = date.split("-");
        String[] months = GlobalDataHolder.getMonthArray();
        String monthInString = months[Integer.parseInt(phrases[1]) - 1];
        String finalDate = phrases[0] + " " + monthInString;

        return finalDate;
    }

    public static String fromUIToDbdateWithYear(String date) {
        String[] tokens = date.split(" ");
        String dbDate = tokens[2] + "-";
        String month = GlobalDataHolder.getMonthsStringToDigit().get(tokens[1]);
        dbDate = dbDate + month + "-" + tokens[0];
        Log.i(LOG_TAG, dbDate);
        Log.i(LOG_TAG, "tokens[2]: " + tokens[2]);
        Log.i(LOG_TAG, date);
        return dbDate;
    }

    public static String fromDbToUIWithoutYear(String date) {
        String[] phrases = date.split("-");
        String[] months = GlobalDataHolder.getMonthArray();
        String monthInString = months[Integer.parseInt(phrases[1]) - 1];
        String finalDate = phrases[2] + " " + monthInString;

        return finalDate;

    }

}
