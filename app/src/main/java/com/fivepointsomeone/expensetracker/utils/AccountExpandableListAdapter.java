package com.fivepointsomeone.expensetracker.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.datamodels.AccountData;
import com.fivepointsomeone.expensetracker.datamodels.AccountExpenseSummary;

import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

public class AccountExpandableListAdapter extends BaseExpandableListAdapter{

    private List<AccountData> accountParentDataList;
    private HashMap<AccountData, AccountExpenseSummary> accountChildDataList;
    private Context context;

    public AccountExpandableListAdapter(List<AccountData> accountParentDataList, HashMap<AccountData, AccountExpenseSummary> accountChildDataList) {

        setAccountParentDataList(accountParentDataList);
        setAccountChildDataList(accountChildDataList);
    }

    public List<AccountData> getAccountParentDataList() {
        return accountParentDataList;
    }

    public void setAccountParentDataList(List<AccountData> accountParentDataList) {
        this.accountParentDataList = accountParentDataList;
    }

    public HashMap<AccountData, AccountExpenseSummary> getAccountChildDataList() {
        return accountChildDataList;
    }

    public void setAccountChildDataList(HashMap<AccountData, AccountExpenseSummary> accountChildDataList) {
        this.accountChildDataList = accountChildDataList;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return getAccountParentDataList().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return getAccountParentDataList().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return getAccountChildDataList().get(getAccountParentDataList().get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_account_expendable_list_parent, parent, false);

        TextView accountName = convertView.findViewById(R.id.textViewAccountName);
        TextView balance = convertView.findViewById(R.id.textViewBalance);

        AccountData accountData = getAccountParentDataList().get(groupPosition);

        accountName.setText(accountData.getName());
        balance.setText(Float.toString(accountData.getBalance()));

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        AccountExpenseSummary accountExpenseSummary = getAccountChildDataList().get(getAccountParentDataList().get(groupPosition));

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_account_expandable_list_child, parent, false);

        TextView monthlyExpense = convertView.findViewById(R.id.textViewMonthlyExpense);
        TextView weeklyExpense = convertView.findViewById(R.id.textViewWeeklyExpense);
        if(accountExpenseSummary != null) {
            monthlyExpense.setText(Float.toString(accountExpenseSummary.getLastMonthNetExpense()));
            weeklyExpense.setText(Float.toString(accountExpenseSummary.getLastWeekNetExpense()));
        } else {
            monthlyExpense.setText("0.00");
            weeklyExpense.setText("0.00");
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

}
