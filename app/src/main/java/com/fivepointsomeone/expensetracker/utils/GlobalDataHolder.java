package com.fivepointsomeone.expensetracker.utils;

import com.fivepointsomeone.expensetracker.datamodels.AccountData;
import com.fivepointsomeone.expensetracker.datamodels.CategoryData;

import java.util.HashMap;

public class GlobalDataHolder {
    private static final CategoryData[] initialCategoryName = {new CategoryData("Other Credit", "CREDIT"), new CategoryData("Other Debit", "DEBIT")};
    private static final AccountData[] initialAccountName = {new AccountData("Other", 0)};
    private static final String[] monthArray = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private static final HashMap<String, String> monthsStringToDigit = new HashMap<>();
    static {
        monthsStringToDigit.put("Jan", "01");
        monthsStringToDigit.put("Feb", "02");
        monthsStringToDigit.put("Mar", "03");
        monthsStringToDigit.put("Apr", "04");
        monthsStringToDigit.put("May", "05");
        monthsStringToDigit.put("Jun", "06");
        monthsStringToDigit.put("Jul", "07");
        monthsStringToDigit.put("Aug", "08");
        monthsStringToDigit.put("Sep", "09");
        monthsStringToDigit.put("Oct", "10");
        monthsStringToDigit.put("Nov", "11");
        monthsStringToDigit.put("Dec", "12");
    }

    public static HashMap<String, String> getMonthsStringToDigit() {
        return monthsStringToDigit;
    }

    public static String[] getMonthArray() {
        return monthArray;
    }

    public static CategoryData[] getInitialCategoryName() {
        return initialCategoryName;
    }

    public static AccountData[] getInitialAccountName() {
        return initialAccountName;
    }
}
