package com.fivepointsomeone.expensetracker.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.datamodels.Transaction;

import java.util.List;

public class TransactionRecyclerViewAdapter extends RecyclerView.Adapter<TransactionRecyclerViewAdapter.TransactionViewHolder>{

    private List<Transaction> transactionList;
    private static final String LOG_TAG = "TransactionRecyclerView";

    public TransactionRecyclerViewAdapter(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    @NonNull
    @Override
    public TransactionRecyclerViewAdapter.TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_oneday_expanse_view,parent, false);
        return new TransactionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position) {

        final Transaction transaction = transactionList.get(position);
        holder.textViewDate.setText(DateAndTimeManipulateFactory.fromDbToUIWithoutYear(transaction.getDate()));
        holder.textViewTime.setText(transaction.getTime());
        holder.textViewReason.setText(transaction.getReason());
        holder.textViewAmount.setText(Float.toString(transaction.getAmount()));
        holder.textViewCategory.setText(transaction.getCategory());
        holder.textViewAccount.setText(transaction.getAccount());

    }

    @Override
    public int getItemCount() {
        try {
            return transactionList.size();
        } catch (NullPointerException e) {
            Log.e(LOG_TAG, "Error while returning itemCount: " + e.getMessage());
            return 0;
        }

    }

    class TransactionViewHolder extends RecyclerView.ViewHolder {

        TextView textViewAmount;
        TextView textViewDate;
        TextView textViewTime;
        TextView textViewReason;
        TextView textViewAccount;
        TextView textViewCategory;


        public TransactionViewHolder(View itemView) {
            super(itemView);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewReason = itemView.findViewById(R.id.textViewReason);
            textViewTime = itemView.findViewById(R.id.textViewTime);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);
            textViewAccount = itemView.findViewById(R.id.textViewAccount);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
        }
    }

}
