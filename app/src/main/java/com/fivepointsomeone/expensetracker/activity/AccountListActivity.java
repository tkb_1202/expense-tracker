package com.fivepointsomeone.expensetracker.activity;


import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.database.DatabaseController;
import com.fivepointsomeone.expensetracker.datamodels.AccountData;
import com.fivepointsomeone.expensetracker.datamodels.AccountExpenseSummary;
import com.fivepointsomeone.expensetracker.utils.AccountExpandableListAdapter;

import java.util.HashMap;
import java.util.List;

public class AccountListActivity extends AppCompatActivity {

    DatabaseController db;
    private ExpandableListView expandableListViewAccountList;
    private List<AccountData> accountList;
    private HashMap<AccountData, AccountExpenseSummary> accountExpenseHashMap;
    private AccountExpandableListAdapter expandableListAdapter;
    private final static String LOG_TAG = "AccountListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        db = DatabaseController.getInstance(getApplicationContext());
        setSupportActionBar(toolbar);
        expandableListViewAccountList = findViewById(R.id.expandableListViewAccountList);
        expandableListViewAccountList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                parent.expandGroup(groupPosition);
                return false;
            }
        });



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(accountList.size() >= 10) {
                    Snackbar.make(view, "You cannot add more accounts", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                } else {
                    launchActivity(AddAccountActivity.class);
                }
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();

        accountList = getAccountListFromDb();
        accountExpenseHashMap = getAccountExpenseHashMap(accountList);
        expandableListAdapter = new AccountExpandableListAdapter(accountList, accountExpenseHashMap);
        expandableListViewAccountList.setAdapter(expandableListAdapter);


    }

    @Override
    public void onBackPressed() {
        launchActivity(HomeActivity.class);
    }

    private void launchActivity(Class activity) {
        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);
    }

    private List<AccountData> getAccountListFromDb() {
        return db.getAccounts();
    }

    private HashMap<AccountData, AccountExpenseSummary> getAccountExpenseHashMap(List<AccountData> accountData) {
        int i = 0;
        HashMap<AccountData, AccountExpenseSummary> hashMap = new HashMap<>();
        while(i < accountData.size()) {
            AccountData data =accountData.get(i);
            hashMap.put(data, new AccountExpenseSummary(0, 0, 0, 0));
            i++;
        }
        return hashMap;
    }

}
