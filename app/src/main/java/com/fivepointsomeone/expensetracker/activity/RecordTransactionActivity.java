package com.fivepointsomeone.expensetracker.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.database.DatabaseController;
import com.fivepointsomeone.expensetracker.datamodels.Transaction;
import com.fivepointsomeone.expensetracker.utils.DateAndTimeManipulateFactory;
import com.fivepointsomeone.expensetracker.utils.EditTextDecimalLimiterFilter;

import java.util.Calendar;
import java.util.List;

public class RecordTransactionActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    private static final String LOG_TAG = "RecordTransaction";

    private Button buttonDate;
    private Button buttonTime;
    private Button buttonAddExpense;
    private EditText editTextReasonForExpense;
    private EditText editTextAmount;

    private Spinner spinnerAccounts;
    private Spinner spinnerCategories;

    private DatabaseController dbController;

    private int year, month, day, hour, minute, second;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_transaction);

        dbController = DatabaseController.getInstance(getApplicationContext());

        buttonTime = findViewById(R.id.timeButton);
        buttonDate = findViewById(R.id.dateButton);
        buttonAddExpense = findViewById(R.id.buttonAddExpense);

        spinnerAccounts = findViewById(R.id.spinnerAccount);
        spinnerCategories = findViewById(R.id.spinnerCategory);

        Calendar cal = Calendar.getInstance();
        String time = DateAndTimeManipulateFactory.fromSystemToUITime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        buttonTime.setText(time);
        String date = DateAndTimeManipulateFactory.fromSystemToUIDateWithYear(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH) + 1), cal.get(Calendar.DAY_OF_MONTH));
        buttonDate.setText(date);

        buttonTime.setOnClickListener(this);
        buttonDate.setOnClickListener(this);
        buttonAddExpense.setOnClickListener(this);

        editTextAmount = findViewById(R.id.editTextAmount);
        editTextReasonForExpense = findViewById(R.id.editTextReason);

        editTextAmount.setFilters(new InputFilter[] {new EditTextDecimalLimiterFilter(3), new InputFilter.LengthFilter(10)});

        spinnerCategories.setOnItemSelectedListener(this);
        spinnerAccounts.setOnItemSelectedListener(this);

        setUpSpinners();

    }

    @Override
    public void onClick(View v) {
        if(v == buttonTime) {
            Calendar calendar = Calendar.getInstance();
            hour = calendar.get(Calendar.HOUR);
            minute = calendar.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    String timeString = DateAndTimeManipulateFactory.fromSystemToUITime(hourOfDay, minute);
                    buttonTime.setText(timeString);

                }
            }, hour, minute, false);
            timePickerDialog.show();


        } else if(v == buttonDate) {

            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String dateString = DateAndTimeManipulateFactory.fromSystemToUIDateWithYear(year, (month+1), dayOfMonth);
                    buttonDate.setText(dateString);
                }
            }, year, month, day);
            datePickerDialog.show();
        } else if(v == buttonAddExpense) {

            boolean isAnythingEmpty = checkIfEmpty();

            if(isAnythingEmpty) {

            } else {
                storeTransaction();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String item = parent.getItemAtPosition(position).toString();
        Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private boolean checkIfEmpty() {
        boolean isThisFirst = true;
        boolean isEmpty = false;
        if(editTextReasonForExpense.getText().toString().isEmpty()) {
            isEmpty = true;
            editTextReasonForExpense.setError("You must add reason");
            editTextReasonForExpense.requestFocus();
            isThisFirst = false;
        } if(editTextAmount.getText().toString().isEmpty()) {
            isEmpty = true;
            editTextAmount.setError("Please enter amount");
            if(isThisFirst) {
                editTextAmount.requestFocus();
            }
        }
        return isEmpty;
    }

    private void setUpSpinners() {
        List<String> listAccounts = dbController.getAccountTypes();
        List<String> listCategories = dbController.getCategoryTypes();

//        Log.i(LOG_TAG, listCategories.get(0));
//        Log.i(LOG_TAG, listCategories.get(0));


        ArrayAdapter<String> adapterForAccounts = new ArrayAdapter<>(this, R.layout.spinner_single_item, listAccounts);
        ArrayAdapter<String> adapterForCategories = new ArrayAdapter<>(this, R.layout.spinner_single_item, listCategories);

        spinnerAccounts.setAdapter(adapterForAccounts);
        spinnerCategories.setAdapter(adapterForCategories);
    }

    private void storeTransaction() {
        String date = DateAndTimeManipulateFactory.fromUIToDbdateWithYear(buttonDate.getText().toString());
        String time = buttonTime.getText().toString();
        String reason = editTextReasonForExpense.getText().toString();
        String stringAmount = editTextAmount.getText().toString();
        if (stringAmount.contains(".")) {
            if (stringAmount.substring(stringAmount.indexOf('.')).length() == 1) {
                stringAmount = stringAmount + "0";
            }
        } else {
            stringAmount = stringAmount + ".0";
        }
        float amount = Float.parseFloat(stringAmount);
        String category = spinnerCategories.getSelectedItem().toString();
        String account = spinnerAccounts.getSelectedItem().toString();
        Transaction transaction = new Transaction(date, time, reason, amount, category, account);
        Log.i(LOG_TAG, transaction.toString());
        boolean isSuccess = dbController.addTransactionToDatabase(transaction);
        if(isSuccess) {

            launchActivity(HomeActivity.class);

        } else {

            Toast.makeText(getBaseContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();

        }
    }

    private void launchActivity(Class activity) {

        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);

    }


}
