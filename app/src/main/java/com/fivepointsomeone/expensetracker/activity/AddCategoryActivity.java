package com.fivepointsomeone.expensetracker.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.database.DatabaseController;
import com.fivepointsomeone.expensetracker.datamodels.CategoryData;

public class AddCategoryActivity extends AppCompatActivity {

    private DatabaseController db;
    private EditText editTextCategoryName;
    private Switch switchCategoryType;
    private Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        db = DatabaseController.getInstance(getApplicationContext());
        editTextCategoryName = findViewById(R.id.editTextCategoryName);
        switchCategoryType = findViewById(R.id.switchCategoryType);
        buttonSubmit = findViewById(R.id.buttonSubmitCategory);

        editTextCategoryName.setFilters(new InputFilter[] {new InputFilter.LengthFilter(20)});
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkIfEmpty()) {
                    addCategoryToDatabase();
                }
            }
        });
    }

    private void addCategoryToDatabase() {

        String categoryName = editTextCategoryName.getText().toString();
        String categoryType;
        if(switchCategoryType.isChecked()) {
           categoryType = "CREDIT";
        } else {
            categoryType = "DEBIT";
        }

        CategoryData categoryData = new CategoryData(categoryName, categoryType);
        boolean isSuccess = db.addCategoryToDatabase(categoryData);
        if(isSuccess) {
            launchActivity(CategoryListActivity.class);
        }

    }

    private boolean checkIfEmpty() {
        boolean isEmpty = false;
        if(editTextCategoryName.getText().toString().isEmpty()) {
            isEmpty = true;
            editTextCategoryName.setError("You must add reason");
            editTextCategoryName.requestFocus();
        }
        return isEmpty;
    }

    private void launchActivity(Class activity) {
        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);
    }

}
