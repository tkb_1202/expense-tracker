package com.fivepointsomeone.expensetracker.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.database.DatabaseController;
import com.fivepointsomeone.expensetracker.datamodels.AccountData;
import com.fivepointsomeone.expensetracker.utils.EditTextDecimalLimiterFilter;

public class AddAccountActivity extends AppCompatActivity implements View.OnClickListener{

    private Button buttonSubmit;
    private EditText editTextAccountName;
    private EditText editTextAccountBalance;
    private DatabaseController db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_account);

        buttonSubmit = findViewById(R.id.buttonSubmitAccount);
        editTextAccountName = findViewById(R.id.editTextCategoryName);
        editTextAccountBalance = findViewById(R.id.editTextAccountBalance);

        editTextAccountName.setFilters(new InputFilter[] {new InputFilter.LengthFilter(20)});
        editTextAccountBalance.setFilters(new InputFilter[] {new EditTextDecimalLimiterFilter(3), new InputFilter.LengthFilter(10)});
        db = DatabaseController.getInstance(getApplicationContext());

        buttonSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v == buttonSubmit) {
            if(!checkIfEmpty()) {
                String name = editTextAccountName.getText().toString();
                float balance = Float.parseFloat(editTextAccountBalance.getText().toString());
                AccountData accountData = new AccountData(name, balance);
                boolean isSuccess = addAccountToDatabase(accountData);
                if(isSuccess) {
                    launchActivity(AccountListActivity.class);
                }
            }

        }
    }

    private boolean checkIfEmpty() {
        boolean isThisFirst = true;
        boolean isEmpty = false;
        if(editTextAccountName.getText().toString().isEmpty()) {
            isEmpty = true;
            editTextAccountName.setError("You must add reason");
            editTextAccountName.requestFocus();
            isThisFirst = false;
        } if(editTextAccountBalance.getText().toString().isEmpty()) {
            isEmpty = true;
            editTextAccountBalance.setError("Please enter amount");
            if(isThisFirst) {
                editTextAccountBalance.requestFocus();
            }
        }
        return isEmpty;
    }

    private boolean addAccountToDatabase(AccountData accountData) {
        return db.addAccountToDatabase(accountData);
    }

    private void launchActivity(Class activity) {
        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);
    }

}
