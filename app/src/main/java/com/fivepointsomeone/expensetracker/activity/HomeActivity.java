package com.fivepointsomeone.expensetracker.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.database.DatabaseController;
import com.fivepointsomeone.expensetracker.datamodels.AccountData;
import com.fivepointsomeone.expensetracker.datamodels.CategoryData;
import com.fivepointsomeone.expensetracker.datamodels.Transaction;
import com.fivepointsomeone.expensetracker.utils.GlobalDataHolder;
import com.fivepointsomeone.expensetracker.utils.TransactionRecyclerViewAdapter;

import java.util.List;
import java.util.prefs.PreferenceChangeEvent;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static final String LOG_TAG = "HomeActivity";

    private DatabaseController dbController;
    private FloatingActionButton fab;
    private ProgressBar progressBar;
    private RecyclerView recyclerViewTransactionList;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbController = DatabaseController.getInstance(getApplicationContext());
        fab = findViewById(R.id.floatingActionButton);
        progressBar = findViewById(R.id.progressBar);
        recyclerViewTransactionList = findViewById(R.id.recyclerViewTransactionList);

        progressBar.setVisibility(View.GONE);
        recyclerViewTransactionList.setVisibility(View.GONE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("firstRun", false)) {
//            insertInitialData();
            new EnterInitialData().execute("");
        } else {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstRun", true);
            editor.commit();
        }

        FloatingActionButton fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new HomeActivity.FetchDataFromSQLite().execute("");
//        afterDataFetched(dbController.getAllTransactionInDateOrder());

//        writeTablesName();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), RecordTransactionActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            View parentLayout = findViewById(R.id.content_home);
            Snackbar.make(parentLayout, "Press back again to Exit", Snackbar.LENGTH_SHORT).show();
            if (doubleBackToExitPressedOnce) {
                finish();
            }

            this.doubleBackToExitPressedOnce = true;

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_accounts) {

            launchActivity(AccountListActivity.class);

        } else if (id == R.id.nav_categories) {

            launchActivity(CategoryListActivity.class);

        } else if (id == R.id.nav_statistics) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class FetchDataFromSQLite extends AsyncTask<String, Object, List<Transaction>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPostExecute(List<Transaction> transactionList) {
            super.onPostExecute(transactionList);
            afterDataFetched(transactionList);

        }

        @Override
        protected List<Transaction> doInBackground(String... strings) {
//        return dbController.getDailyExpensesForAllDays();
            return dbController.getAllTransactionInDateOrder();
//            return null;
        }
    }

    private class EnterInitialData extends AsyncTask<Object, Object, Object> {

        @Override
        protected Object doInBackground(Object... objects) {
            insertInitialData();
            return null;
        }
    }

    private void afterDataFetched(List<Transaction> transactionList) {
        progressBar.setVisibility(View.GONE);
        recyclerViewTransactionList.setVisibility(View.VISIBLE);

        TransactionRecyclerViewAdapter transactionRecyclerViewAdapter = new TransactionRecyclerViewAdapter(transactionList);
        LinearLayoutManager l = new LinearLayoutManager(getBaseContext());
        recyclerViewTransactionList.setAdapter(transactionRecyclerViewAdapter);
        l.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewTransactionList.setLayoutManager(l);
    }

    private void launchActivity(Class activity) {
        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);
    }

    private void insertInitialData() {

        Log.i(LOG_TAG, "insertInitialData begun");

        int i = 0;
        AccountData[] initialAccountName = GlobalDataHolder.getInitialAccountName();
        CategoryData[] initialCategoryName = GlobalDataHolder.getInitialCategoryName();
        Log.i(LOG_TAG, "Adding accounts:\n");
        while(i < initialAccountName.length) {
            boolean isSuccess = dbController.addAccountToDatabase(initialAccountName[i]);
            Log.i(LOG_TAG, Integer.toString(i) + ": " + Boolean.toString(isSuccess));
            i++;

        }
        Log.i(LOG_TAG, "Size of i before: " + Integer.toString(i));
        i = 0;
        Log.i(LOG_TAG, "Adding categories:\n");
        while(i < initialCategoryName.length) {
            boolean isSuccess = dbController.addCategoryToDatabase(initialCategoryName[i]);
            Log.i(LOG_TAG, Integer.toString(i) + ": " + Boolean.toString(isSuccess));
            i++;
        }

        Log.i(LOG_TAG, "Size of i after: " + Integer.toString(i));

    }



}
