package com.fivepointsomeone.expensetracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.fivepointsomeone.expensetracker.R;
import com.fivepointsomeone.expensetracker.database.DatabaseController;
import com.fivepointsomeone.expensetracker.datamodels.CategoryData;
import com.fivepointsomeone.expensetracker.datamodels.CategoryExpenseSummary;
import com.fivepointsomeone.expensetracker.utils.CategoryExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryListActivity extends AppCompatActivity {

    private static final String LOG_TAG = "CategoryListActivity";
    DatabaseController db;
    ExpandableListView expandableListViewDebitCategories;
    ExpandableListView expandableListViewCreditCategories;
    List<CategoryData> creditCategoryList;
    List<CategoryData> debitCategoryList;
    HashMap<CategoryData, CategoryExpenseSummary> creditExpanseSummaryHashMap;
    HashMap<CategoryData, CategoryExpenseSummary> debitExpanseSummaryHashMap;
    CategoryExpandableListAdapter expandableListAdapterCredit;
    CategoryExpandableListAdapter expandableListAdapterDebit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = DatabaseController.getInstance(getApplicationContext());
        expandableListViewDebitCategories = findViewById(R.id.expandableListViewDebitCategories);
        expandableListViewCreditCategories = findViewById(R.id.expandableListViewCreditCategories);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivity(AddCategoryActivity.class);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        creditCategoryList = new ArrayList<>();
        debitCategoryList = new ArrayList<>();
        List<CategoryData> allCategories = getCategoryListFromDb();
        Log.i(LOG_TAG, "Category List size: " + Integer.toString(allCategories.size()));
        int i = 0;
        while(i < allCategories.size()) {
            CategoryData categoryData = allCategories.get(i);
            if(categoryData.getType().equals("CREDIT")) {
                creditCategoryList.add(categoryData);
            } else {
                debitCategoryList.add(categoryData);
            }
            i++;
        }

        creditExpanseSummaryHashMap = getCategoryExpanseHashMap(creditCategoryList);
        debitExpanseSummaryHashMap = getCategoryExpanseHashMap(debitCategoryList);

        expandableListAdapterDebit = new CategoryExpandableListAdapter(debitCategoryList, debitExpanseSummaryHashMap);
        expandableListAdapterCredit = new CategoryExpandableListAdapter(creditCategoryList, creditExpanseSummaryHashMap);

        expandableListViewDebitCategories.setAdapter(expandableListAdapterDebit);
        expandableListViewCreditCategories.setAdapter(expandableListAdapterCredit);

    }

    @Override
    public void onBackPressed() {
        launchActivity(HomeActivity.class);
    }

    private void launchActivity(Class activity) {
        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);
    }

    private List<CategoryData> getCategoryListFromDb() {
        return db.getCategories();
    }

    private HashMap<CategoryData, CategoryExpenseSummary> getCategoryExpanseHashMap(List<CategoryData> categoryDataList) {
        HashMap<CategoryData, CategoryExpenseSummary> hashMap = new HashMap<>();
        int i = 0;
        while(i < categoryDataList.size()) {
            CategoryData categoryData = categoryDataList.get(i);
            CategoryExpenseSummary categoryExpenseSummary = new CategoryExpenseSummary(0, 0);
            hashMap.put(categoryData, categoryExpenseSummary);
            i++;
        }

        return hashMap;
    }

}
